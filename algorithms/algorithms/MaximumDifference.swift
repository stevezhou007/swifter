//
//  MaximumDifference.swift
//  algorithms
//
//  Created by xiqi zhou on 9/13/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation
// Bad code below
func getMaxDifference(numbers: [Int]) -> Int? {
    
   guard numbers.count >= 2 else { return nil }
   
   var difference = numbers[1] - numbers[0]
   var currentSum = difference
   var maxSum = currentSum
    
    for i in 0...numbers.count - 1 {
        
        difference =  numbers[i + 1] - numbers[i]
        currentSum = currentSum > 0 ? currentSum + difference : difference
        if currentSum > maxSum {
            maxSum = currentSum
        }
    }
    
   return maxSum
}


