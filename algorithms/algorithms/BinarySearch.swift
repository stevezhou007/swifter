//
//  BinarSearch.swift
//  algorithms
//
//  Created by xiqi zhou on 9/1/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation


func binarSearch(elements: [Int], value: Int) -> Bool {
    var l = 0
    var r = elements.count - 1
    var m = l + ((r - l) / 2)
  
    while (l <= r) {
        
        m = l + ((r - l) / 2)
        
        print("M: \(m)")
        
        if value == elements[m] { return true}
        
        if value > elements[m] {
            l = m + 1
        }else{
            r =  m - 1
        }
    }
    
    return false 
}
