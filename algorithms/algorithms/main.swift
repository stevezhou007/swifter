//
//  main.swift
//  algorithms
//
//  Created by xiqi zhou on 9/1/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation

func getOddOccurenceNumber(numbers: [Int]) -> Int {
    
    var result = 0
    for i in 0...numbers.count - 1 {
        result = result ^ numbers[i]
    }
   return result
}


let result = getOddOccurenceNumber(numbers: [1,2,4,4,4,2,1,2,3,4,4])
print(result)
