//
//  OOP.swift
//  algorithms
//
//  Created by xiqi zhou on 9/7/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation

//Moore Voting Algorithm

func MooreVotingAlgorithm(numbers: [Int]) -> Int? {
    
    var voteNumber = 0
    var votes = 1
    
    for i in 1...numbers.count - 1 {
        
        if numbers[voteNumber] == numbers[i] {
            votes += 1
        }else {
            votes -= 1
        }
        
        if votes == 0 {
            
            voteNumber = i
            votes = 1
        }
        
    }
 
    return numbers.filter { $0 == numbers[voteNumber] }.count > (numbers.count / 2) ? numbers[voteNumber] : nil
    
}
