//
//  FindPairs.swift
//  algorithms
//
//  Created by xiqi zhou on 9/1/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation


func findAllPairs(numbers: [Int], sumValue: Int) {
   
    var hashDictionary = [Int:Int]()
    
    for i in 0...numbers.count - 1 {
        
        let result = sumValue - numbers[i]
        
        if hashDictionary[result] == 1 {
            print("Sum Pair: (\(result), \(numbers[i]))")
        }
       hashDictionary[numbers[i]] = 1
    }
} //let numbers = [2,3,4,5,6,7,8,9], sum = 8
