//: Playground - noun: a place where people can play


import UIKit

protocol Person {
    
    associatedtype Professional
    
    mutating func myProfession() -> Professional
    
}


struct Programmer: Person {
  mutating func myProfession() -> String {
        return "I am a programmer"
    }
}

//let programmer: Person = Programmer()

class AnyProfessionBase<A>: Person {
    
    func myProfession() -> A {
        fatalError("This is an abstract method")
    }
}


class AnyProfessionWrapper<Human: Person>: AnyProfessionBase<Human.Professional> {
    
    var person: Human
    
    init(_ person: Human) {
        self.person = person
    }
    
    override func myProfession() -> Human.Professional {
        
        return person.myProfession()
    }
    
}

let person: AnyProfessionBase<String> = AnyProfessionWrapper(Programmer())

print(person.myProfession())

//let person: Person = Programmer()


protocol Row {
    
    associatedtype Model
    
    var textLabel: String {get set}
    
    func configure(item: Model)
}


struct Folder {}
struct File {}

struct FileCell: Row {
    
    typealias Model = File
    
    var textLabel: String = ""
    
    func configure(item: Model) { print("This File Cell")}
}

struct AnotherFileCell: Row {
      typealias Model = File
    
    var textLabel: String = "Another File Cell"
    
    func configure(item: File) {
        print("Another File Cell")
    }
    
}


 class _AnyRowBase<WOW>: Row {
    
    init() {
        
        guard type(of: self) != _AnyRowBase.self else { fatalError("You should create a subclass instead")}
    }
    
    var textLabel: String{
        
        get{  fatalError("This is the abstract get method")}
        set{  fatalError("This is the abstract set method")}
    }
    
    func configure(item: WOW) {
        print("This is a abstract method")
    }
        
}

private class _AnyRowBox<Concrete: Row>: _AnyRowBase<Concrete.Model> {
    
    var concrete: Concrete
    
    init(_ concrete: Concrete) {
        self.concrete = concrete
    }
    
    override var textLabel: String {
        get {
            return concrete.textLabel
        }
        set {
            concrete.textLabel = newValue
        }
    }
    
    override func configure(item: Concrete.Model) {
        concrete.configure(item: item)
    }
    
}


class AnyBase<Idontknow>: Row {
    
    private let box: _AnyRowBase<Idontknow>
    
    init<Concrete: Row>(_ concrete: Concrete) where Concrete.Model == Idontknow {
        box = _AnyRowBox(concrete)
    }
    
    var textLabel: String {
        
        get{
            return box.textLabel
        }
        
        set {
            box.textLabel = newValue
        }
    }
    
    func configure(item: Idontknow) {
     
        box.configure(item: item)
    }
    
}




































