//
//  QuestionViewController.swift
//  LifeHack
//
//  Created by xiqi zhou on 7/20/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        print("A view controller from a storyboard gets initialized in the init coder")
    }
    override func loadView() {
        super.loadView()
        print("The view is being loaded in memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
         print("The view did load and is now accessible, but it still not visible on the screen")
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("The view is about to appear on screen")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("The view appeared on screen")
    }


}
