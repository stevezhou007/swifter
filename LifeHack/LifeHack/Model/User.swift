//
//  User.swift
//  LifeHack
//
//  Created by xiqi zhou on 7/20/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation
import UIKit

struct User {
    
    let name: String
    let aboutMe: String
    let profilePictureName: String
    let reputation: Int
}
