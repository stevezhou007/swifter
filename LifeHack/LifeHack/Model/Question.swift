//
//  Question.swift
//  LifeHack
//
//  Created by xiqi zhou on 7/20/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation
import UIKit

struct Question {
    
    let title: String
    let body: String
    let score: Int
    let owner: User
}
