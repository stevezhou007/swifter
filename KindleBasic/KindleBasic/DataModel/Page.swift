//
//  Page.swift
//  KindleBasic
//
//  Created by xiqi zhou on 9/16/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation

struct Page: Decodable {
    
    let id: Int
    let text: String
}
