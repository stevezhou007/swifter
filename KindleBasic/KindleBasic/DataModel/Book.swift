//
//  Book.swift
//  KindleBasic
//
//  Created by xiqi zhou on 9/16/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation
import UIKit

struct Book: Decodable {
    
    let id: Int
    let author: String
    let coverImageUrl: String
    let title: String
    let pages: [Page]?
}

