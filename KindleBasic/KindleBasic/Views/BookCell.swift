//
//  BookCell.swift
//  KindleBasic
//
//  Created by xiqi zhou on 9/16/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import UIKit

class BookCell: UITableViewCell {
    
    var book: Book? {
        
        didSet{
            authorLabel.text = book?.author
            titleLabel.text = book?.title
            BookData.fetchOnlineImage(imageUrl: book?.coverImageUrl) { (image) in
                
                DispatchQueue.main.async {
                    self.coverImageView.image = nil 
                    self.coverImageView.image = image
                }
            }
        }
    }
    
 private   let coverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
private    let authorLabel: UILabel = {
         let author = UILabel()
         author.translatesAutoresizingMaskIntoConstraints = false
         author.textColor = .lightGray
         author.font = UIFont.systemFont(ofSize: 16)
        return author
    }()
    
private  let titleLabel: UILabel = {
        let title = UILabel()
        title.translatesAutoresizingMaskIntoConstraints = false
        title.textColor = .white
        title.font = UIFont.boldSystemFont(ofSize: 16)
        return title
    }()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        addSubview(coverImageView)
        coverImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        coverImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        coverImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8).isActive = true
        coverImageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        
        addSubview(titleLabel)
        titleLabel.leftAnchor.constraint(equalTo: coverImageView.rightAnchor, constant: 8).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -10).isActive = true
        
        addSubview(authorLabel)
        
        authorLabel.leftAnchor.constraint(equalTo: coverImageView.rightAnchor, constant: 8).isActive = true
        authorLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        authorLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        authorLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
