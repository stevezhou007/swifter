//
//  PageCell.swift
//  KindleBasic
//
//  Created by xiqi zhou on 9/16/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import UIKit


class PageCell: UICollectionViewCell {
    
    var textLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(white: 0.8, alpha: 0.3)
        addSubview(textLabel)
        textLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        textLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        textLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 8).isActive = true
        textLabel.heightAnchor.constraint(equalToConstant: 20)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
