//
//  ViewController.swift
//  KindleBasic
//
//  Created by xiqi zhou on 9/16/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//
import Foundation
import UIKit

class ViewController: UITableViewController {
   
    var books: [Book]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       navigationItem.title = "Kindle"
        tableView.register(BookCell.self, forCellReuseIdentifier: "cellId")
        tableView.tableFooterView = UIView()
        setupNavigationBarStyles()
        setupNavBarButtion()
        tableView.backgroundColor = UIColor(white: 0.8, alpha: 0.3)
        tableView.separatorColor = UIColor(white: 1, alpha: 0.2)
        
        
        BookData.fetchBookOnline { (data) in
            DispatchQueue.main.async {
                    self.books = data
            self.tableView.reloadData()
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
       let footerview = UIView()
        footerview.backgroundColor = UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha: 1)
        let segementedControl = UISegmentedControl(items: ["Cloud", "Device"])
            footerview.addSubview(segementedControl)
        
            segementedControl.tintColor = .white
            segementedControl.selectedSegmentIndex = 0
        
            segementedControl.translatesAutoresizingMaskIntoConstraints = false
            segementedControl.widthAnchor.constraint(equalToConstant: 200).isActive = true
            segementedControl.heightAnchor.constraint(equalToConstant: 30).isActive = true
            segementedControl.centerXAnchor.constraint(equalTo: footerview.centerXAnchor).isActive = true
            segementedControl.centerYAnchor.constraint(equalTo: footerview.centerYAnchor).isActive = true
        
        
        let gridButton = UIButton(type: .system)
            gridButton.setImage(#imageLiteral(resourceName: "grid").withRenderingMode(.alwaysOriginal), for: .normal)
            gridButton.translatesAutoresizingMaskIntoConstraints = false
            footerview.addSubview(gridButton)
        
            gridButton.leftAnchor.constraint(equalTo: footerview.leftAnchor).isActive = true
            gridButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
            gridButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            gridButton.centerYAnchor.constraint(equalTo: footerview.centerYAnchor).isActive = true
        
        let sortButton = UIButton(type: .system)
            sortButton.setImage(#imageLiteral(resourceName: "sort").withRenderingMode(.alwaysOriginal), for: .normal)
            sortButton.translatesAutoresizingMaskIntoConstraints = false
            footerview.addSubview(sortButton)
        
            sortButton.rightAnchor.constraint(equalTo: footerview.rightAnchor).isActive = true
            sortButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
            sortButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            sortButton.centerYAnchor.constraint(equalTo: footerview.centerYAnchor).isActive = true
        
        return footerview
    }
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        return books?.count ?? 0
    }
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! BookCell
          let data = books?[indexPath.row]
          cell.book = data
          cell.selectionStyle = .none
          return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let book = books?[indexPath.row]
        
        let flowLayout = UICollectionViewFlowLayout()
        let bookCollectionView = BookPagesController(collectionViewLayout: flowLayout)
            bookCollectionView.pages = book?.pages
        
        let navigationController = UINavigationController(rootViewController: bookCollectionView)
        present(navigationController, animated: true , completion: nil)
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func setupNavigationBarStyles() {
        navigationController?.navigationBar.barTintColor = UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha: 1)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    func setupNavBarButtion() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleMenuPress))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "amazon_icon").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleAmazonButtonPress))
    }
   
   @objc func handleMenuPress() {
        
        print("Menu button pressed")
    }

    @objc func handleAmazonButtonPress() {
        print("Amazon button pressed")
    }
}

