//
//  BookData.swift
//  KindleBasic
//
//  Created by xiqi zhou on 9/16/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation
import UIKit

struct BookData  {
    
//    static func bookData() -> [Book] {
//
//        var books: [Book] = []
//
//        let page1 = Page(id: 1, text: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.")
//        let page2 = Page(id: 2, text: "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum ")
//        let pages = [page1, page2]
//
//        let book1 = Book(title: "Steve Jobs", author: "Walter Isaacson", image: #imageLiteral(resourceName: "steve_jobs"), pages: pages)
//
//        let book2 = Book(title: "Bill Gates: A Biography", author: "Michael Becraft", image: #imageLiteral(resourceName: "bill_gates"), pages: [
//              Page(id: 1, text: "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.  fringilla vel, aliquet nec, vulputate eget, arcu.  Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum "),
//              Page(id: 2, text: "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enilputate eget, arcu. In enatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum "),
//              Page(id: 3, text: "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla cons eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum "),
//              Page(id: 4, text: "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Null ")
//            ])
//
//           books.append(contentsOf: [book1, book2])
//
//        return books
//
//    }
    
    
    static func fetchBookOnline(completionHandler: @escaping ([Book]) -> Void){
        
        if let url = URL(string: "https://letsbuildthatapp-videos.s3-us-west-2.amazonaws.com/kindle.json") {
            
            URLSession.shared.dataTask(with: url, completionHandler: { (data , repsponse, error) in
                
                if (error) != nil {
                    print("Something wrong with the link")
                }
                
                guard let datas = data else { return }
                
                do{
                   
                    let jsondata = try JSONDecoder().decode([Book].self, from: datas)
                     completionHandler(jsondata)
                    
                }catch{
                    
                }
                
                
            }).resume()
            
        }
        
    } // end of Fetch book online Data Method
    
    static func fetchOnlineImage(imageUrl: String?, completionHandler: @escaping (UIImage) -> Void) {
        guard let imageurl = imageUrl else { return }
        if let url = URL(string: imageurl) {
            
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if let err = error {
                    print("Failed to retrieve book cover image", err)
                    return
                }
                
                guard let imageData = data else {return }
                guard let image = UIImage(data: imageData) else {return }
                completionHandler(image)
                
                
                
            }).resume()
        }
    
        
    }// end of fetch on line image method
}


