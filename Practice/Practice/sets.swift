//
//  File.swift
//  Practice
//
//  Created by xiqi zhou on 7/27/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation

enum ItemType: String{
    
    case Refrigerator = "Refrigerator"
    case Microwaves = "Microwave"
    case WashersAndDryers = "Washer&Dryer"
    case Ranges = "Range"
    case Tools = "Tool"
    case Grill = "Grill"
    case Flowers = "Flower"
}

