//
//  Inventory.swift
//  Practice
//
//  Created by xiqi zhou on 7/27/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation

struct Inventory: CustomStringConvertible {
    
    final class Item {
        
        var name: ItemType
        var cost: ItemCost
        var barcode: String
        var comment: String
        
        
        init(name: ItemType, cost: ItemCost, barcode: String, comment: String) {
            self.name = name
            self.cost = cost
            self.barcode = barcode
            self.comment = comment
        }
        
        func copy() -> Item {
            
            return Item(name: name, cost: cost, barcode: barcode, comment: comment)
        }
        
    }//end of final class
    
    var name: ItemType {
        get {
            return _item.name
        }
        set {
            _mutatingItem.name = newValue
        }
    }
    
    var cost: ItemCost {
        get {
            return _item.cost
        }
        set {
            _mutatingItem.cost = newValue
        }
    }
    
    var barcode: String {
        
        get {
            
            return _item.barcode
        }
        
        set {
            
            _mutatingItem.barcode = newValue
        }
    }
    
    var comment: String {
        
        get {
            return _item.comment
        }
        set {
            _mutatingItem.comment = newValue
        }
    }
    
    
    fileprivate var _item: Item
    
    fileprivate var _mutatingItem: Item {
        
        mutating get {
            if !isKnownUniquelyReferenced(&_item) {
                
                _item = _item.copy()
            }
            
            return _item
        }
    }
    
    var description: String {
        
        return "name: \(name), cost: \(cost.price), barcode: \(barcode)"
    }
    
    init(item: Item) {
        self._item = item 
    }
    
    
}
