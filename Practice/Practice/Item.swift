//
//  Item.swift
//  Practice
//
//  Created by xiqi zhou on 7/27/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation


class ItemCost: CustomStringConvertible {
    
    fileprivate var _quantity: Int
    fileprivate var _type: ItemType
    
    var quantity: Int {
        get {
            return _quantity
        }
        set {
            _quantity = newValue
        }
    }
    
    var itemType: ItemType {
        
        get {
            return _type
        }
        
        set {
            
            _type = newValue
        }
    }
    
    var price: Double {
        
        switch _type {
        case .Flowers: return 10.5 * Double(_quantity)
        case .Grill: return 135 * Double(_quantity)
        case .Microwaves: return 99.99 * Double(_quantity)
        case .Ranges: return 235.99 * Double(_quantity)
        case .Refrigerator: return 315.99 * Double(_quantity)
        case .Tools: return 16.99 * Double(_quantity)
        case .WashersAndDryers: return 355.99 * Double(_quantity)
        }
    }
    
    init(quantity: Int, itemType: ItemType) {
        self._quantity = quantity
        self._type = itemType
    }
    
    var description: String {
        
        return "name: \(_type): Quantity: \(_quantity)"
    }
    
}


