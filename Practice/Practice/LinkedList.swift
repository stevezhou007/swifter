//
//  LinkedList.swift
//  Practice
//
//  Created by xiqi zhou on 7/28/17.
//  Copyright © 2017 xiqi zhou. All rights reserved.
//

import Foundation

class LinkedList<T> {
    
    var item: T
    var next: LinkedList<T>?
    
    init(item: T, next: LinkedList<T>?) {
        self.item = item
        self.next = next
    }
    
    
    
}


struct LinkedListIterator<T>: IteratorProtocol {
    
    var current: LinkedList<T>
    
    mutating func next() -> T? {
        
        if current.next != nil {
            current = current.next!
            return current.item
        }else {
            return nil
        }
        
    }
    
}

extension LinkedList: Sequence {
    
    func makeIterator() -> LinkedListIterator<T> {
        return LinkedListIterator(current: self)
    }
}




